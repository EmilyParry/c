#include "stdafx.h"
#include "LeaderBoard.h"
#include "functions.h"
#include "Word.h"
#include <string> 

//code code code

void wordIn(vector<Word>& easy, vector<Word>& medium, vector<Word>& hard); 
int letterFill(char, string, string&);
int levelOne(vector<Word>& word);
int levelTwo(vector<Word>& word);
void leaderboardIn(vector<LeaderBoard>& leaderVector);
void saveLeader(vector<LeaderBoard>& leaderVector);
void addLeader(vector<LeaderBoard>& leaderVector, int score);
void PlayHangman(vector<Word> easyVector, vector<Word> mediumVector);
void DisplayLeaderBoard(vector<LeaderBoard>& leaderVector);

int main()
{	
	vector<Word> easyVector;
	vector<Word> mediumVector;
	vector<Word> hardVector;
	vector<LeaderBoard> leaderVector;
	wordIn(easyVector, mediumVector, hardVector);
	leaderboardIn(leaderVector);
	
	cout << "\n\nWelcome to Hangman." << endl;
	int score = 0;
	int choice;

	for (;;)
	{
		cout << "What would you like to do: 1) Play Hangman, 2)Display Leaderboard" << endl;
		cin >> choice;

		if (choice == 1)
		{
			PlayHangman(easyVector, mediumVector);
		}

		else if (choice == 2)
		{
			DisplayLeaderBoard(leaderVector);
		}
		else
		{
			cout << "Invalid Input" << endl;
		}
		
		cout << "Repeat? [y/n] " << endl;
		char answer;	
		cin >> answer;

		if (answer == 'n')
		{
			addLeader(leaderVector, score);
			saveLeader(leaderVector);
			break;// exit 
		}

		else if (answer != 'y' && answer != 'n')
		{
			cout << "Invalid Input" << endl;
			addLeader(leaderVector, score);
			saveLeader(leaderVector);
			break;
		}
		score++;
	}
	cout << "Goodbye!" << endl;
    return 0;
}

void wordIn(vector<Word>& easy, vector<Word>& medium, vector<Word>& hard) // Take in strings from file and insert into easy medium and hard word vector
{

	string oneWord;
	string word;
	Word newWord;
	
	ifstream fin("wordData.txt"); 
	ofstream fout("output.txt");
	if (!fin || !fout)
	{
		cout << "failed to open file";
	}

	do 
	{
		word = " ";
		do
		{
			fin >> oneWord;
			word += oneWord;
			if (oneWord[oneWord.size() - 1] != ';')
			{
				word += ' ';
			}
			
		} while (oneWord[oneWord.size() - 1] != ';');
		word[word.size() - 1] = ' ';
		newWord.setWord(word);
		easy.push_back(newWord);
	} while (!fin.eof());
	fin.close();

	// Medium word List
	ifstream med("mediumData.txt");
	ofstream fileout("output.txt");
	if (!med || !fileout)
	{
		cout << "failed to open file";
	}
	do
	{
		word = " ";
		do
		{
			med >> oneWord;
			word += oneWord;
			if (oneWord[oneWord.size() - 1] != ';')
			{
				word += ' ';
			}

		} while (oneWord[oneWord.size() - 1] != ';');
		word[word.size() - 1] = ' ';
		newWord.setWord(word);
		medium.push_back(newWord);
	} while (!med.eof());
	med.close();

	// Hard
	ifstream h("hardData.txt");
	ofstream fileout2("output.txt");
	if (!h || !fileout2)
	{
		cout << "failed to open file";
	}
	do
	{
		word = " ";
		do
		{
			h >> oneWord;
			word += oneWord;
			if (oneWord[oneWord.size() - 1] != ';')
			{
				word += ' ';
			}

		} while (oneWord[oneWord.size() - 1] != ';');
		word[word.size() - 1] = ' ';
		newWord.setWord(word);
		hard.push_back(newWord);
	} while (!h.eof());
	h.close();

}
void leaderboardIn(vector<LeaderBoard>& leaderVector) // Take in strings from file and insert into word vector
{

	string oneWord;
	string newName;
	int newScore;
	LeaderBoard newPlayer;

	ifstream readIn("LeaderBoard.txt"); // ; at the end of each name.
								  // Each player on new line
	ofstream fout("output.txt");
	if (!readIn || !fout)
	{
		cout << "failed to open file";

	}
	do
	{
		newName = "";
		do
		{
			readIn >> oneWord;
			newName += oneWord;
			if (oneWord[oneWord.size() - 1] != ';')
			{
				newName += ' ';
			}
		}
		while (oneWord[oneWord.size() - 1] != ';');
		newName[newName.size() - 1] = ' ';
		readIn >> newScore;
		
		newPlayer.setName(newName);
		newPlayer.setScore(newScore);
		leaderVector.push_back(newPlayer);

	} while (!readIn.eof());
	readIn.close();
	

}
void addLeader(vector<LeaderBoard>& leaderVector, int score)
{
	string addName;
	LeaderBoard player;

	cout << "Name : " << endl;
	cin >> addName;
	player.setName(addName);
	player.setScore(score);

	leaderVector.push_back(player);

}
void saveLeader(vector<LeaderBoard>& leaderVector)
{
	ofstream writeLeader("LeaderBoard.txt");
	string name;
	int score;
	if (writeLeader)
	{
		for (LeaderBoard thisLead : leaderVector)
		{
			name = thisLead.getName();
			score = thisLead.getScore();
			writeLeader << name + "; " << score << endl;
		}
		writeLeader.close();
	}
	else
	{
		cout << "Error opening file" << endl;
	}
}
int levelOne(vector<Word>& word)
{
	//	srand(time(NULL));

	int NumberOfWrongGuesses = 0;
	char letter;
	int i = 1;

	//	srand(time(NULL));

	//vector<Word> wordList;
	
	//wordIn(wordList);

	//put all the string inside the array here
	string chosenWord;
	Word wordchose;
	chosenWord = "help";
	cout << "There are " << word.size() << " elements in the array" << endl;

	int n = rand() % word.size();
	//wordchose = word[n];

	//call the function here for guessing game
	// Initialize the secret word with the * character.
	string unknown(chosenWord.length(), '*');
	cout << "\n\nEach letter is represented by an asterisk.";
	cout << "\n\nPlease type one letter at a time.";
	cout << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	// Loop until the guesses are used up
	while (NumberOfWrongGuesses < 9)
	{
		cout << "\n\n" << unknown;
		cout << "\n\nGuess a letter: ";
		cin >> letter;
		// Fill secret word with letter if the guess is correct,
		// otherwise increment the number of wrong guesses.
		if (letterFill(letter, chosenWord, unknown) == 0)
		{
			cout << endl << "Whoops! That letter isn't in there!" << endl;
			NumberOfWrongGuesses++;
		}
		else
		{
			cout << endl << "You found a letter! Isn't that exciting?" << endl;
		}
		// Tell user how many guesses has left.
		cout << "You have " << 9 - NumberOfWrongGuesses;
		cout << " guesses left." << endl;
		// Check if user guessed the word.
		if (chosenWord == unknown)
		{
			cout << chosenWord << endl;
			cout << "Yeah! You got it!";
			break;
		}
#pragma region Graphics

		if (NumberOfWrongGuesses == 1)
		{
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
		}
		else if (NumberOfWrongGuesses == 2)
		{
			cout << "|===" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
		}
		
		else if (NumberOfWrongGuesses == 3)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
		
		
		}
		else if (NumberOfWrongGuesses == 4)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|  O" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
		}
		else if (NumberOfWrongGuesses == 5)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|  O" << endl;
			cout << "|  |" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
		}
		else if (NumberOfWrongGuesses == 6)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|  O" << endl;
			cout << "| -|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
		}
		else if (NumberOfWrongGuesses == 7)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|  O" << endl;
			cout << "| -|-" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
		}
		else if (NumberOfWrongGuesses == 8)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|  O" << endl;
			cout << "| -|-" << endl;
			cout << "| /" << endl;
			cout << "|" << endl;
		}
		else if (NumberOfWrongGuesses == 9)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|  O" << endl;
			cout << "| -|-" << endl;
			cout << "| / \\ " << endl;
		}
#pragma endregion
	}
	if (NumberOfWrongGuesses == 9)
	{
		system("color A4");
		cout << "\nSorry, you lose...you've been hanged." << endl;
		cout << "The word was : " << chosenWord << endl;
	}
	cin.ignore();
	cin.get();
	return 0;
}
int levelTwo(vector<Word>& word)
{
	//	srand(time(NULL));

	int NumberOfWrongGuesses = 0;
	char letter;
	int i = 1;
	//put all the string inside the array here
	string chosenWord;
	Word wordchose;
	//chosenWord = "help";
	cout << "There are " << word.size() << " elements in the array" << endl;

	srand((unsigned)time(0));
	int n = rand() % (word.size() - 1);
	wordchose = word[n];
	chosenWord = wordchose.getWord();
	cout << chosenWord << endl;
	//call the function here for guessing game
	// Initialize the secret word with the * character.
	string unknown(chosenWord, '*');
	cout << "\n\nEach letter is represented by an asterisk.";
	cout << "\n\nPlease type one letter at a time.";
	cout << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	// Loop until the guesses are used up
	while (NumberOfWrongGuesses < 9)
	{
		cout << "\n\n" << unknown;
		cout << "\n\nGuess a letter: ";
		cin >> letter;
		// Fill secret word with letter if the guess is correct,
		// otherwise increment the number of wrong guesses.
		if (letterFill(letter, chosenWord, unknown) == 0)
		{
			cout <<  "Whoops! That letter isn't in there!" << endl;
			NumberOfWrongGuesses++;
		}
		else
		{
			cout << endl << "You found a letter! Isn't that exciting?" << endl;
		}
		// Tell user how many guesses has left.
		cout << "You have " << 9 - NumberOfWrongGuesses;
		cout << " guesses left." << endl;
		// Check if user guessed the word.
		if (chosenWord == unknown)
		{
			cout << chosenWord << endl;
			cout << "Yeah! You got it!";
			break;
		}
#pragma region Graphics

		if (NumberOfWrongGuesses == 1)
		{
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
		}
		else if (NumberOfWrongGuesses == 2)
		{
			cout << "|===" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
		}

		else if (NumberOfWrongGuesses == 3)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;


		}
		else if (NumberOfWrongGuesses == 4)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|  O" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
		}
		else if (NumberOfWrongGuesses == 5)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|  O" << endl;
			cout << "|  |" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
		}
		else if (NumberOfWrongGuesses == 6)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|  O" << endl;
			cout << "| -|" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
		}
		else if (NumberOfWrongGuesses == 7)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|  O" << endl;
			cout << "| -|-" << endl;
			cout << "|" << endl;
			cout << "|" << endl;
		}
		else if (NumberOfWrongGuesses == 8)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|  O" << endl;
			cout << "| -|-" << endl;
			cout << "| /" << endl;
			cout << "|" << endl;
		}
		else if (NumberOfWrongGuesses == 9)
		{
			cout << "|===" << endl;
			cout << "|  !" << endl;
			cout << "|  O" << endl;
			cout << "| -|-" << endl;
			cout << "| / \\ " << endl;
		}
#pragma endregion
	}
	if (NumberOfWrongGuesses == 9)
	{
		system("color A4");
		cout << "\nSorry, you lose...you've been hanged." << endl;
		cout << "The word was : " << chosenWord << endl;
	}
	cin.ignore();
	cin.get();
	return 0;
}
int letterFill(char guess, string secretword, string &guessword)
{
	int i;
	int matches = 0;
	int len = secretword.length();
	for (i = 0; i< len; i++)
	{
		// Was this letter guesed already?
		if (guess == guessword[i])
			return 0;
		// Is the guess in the secret word?
		if (guess == secretword[i])
		{
			guessword[i] = guess;
			matches++;
		}
	}
	return matches;
}
void PlayHangman(vector<Word> easyVector, vector<Word> mediumVector)
{
	cout << "\nChoose a Difficulty Level : " << endl;
	cout << "\n1) Easy" << endl;
	cout << "\n2) Medium" << endl;
	cout << "\n3) Hard" << endl;
	int level;
	cin >> level;
	if (level == 1)
	{
		levelOne(easyVector);
	}

	else if (level == 2)
	{
		levelTwo(mediumVector);
	}

	else
	{
		cout << "Invalid Input" << endl;
	}

}
void DisplayLeaderBoard(vector<LeaderBoard>& leaderVector)
{
	for (LeaderBoard thisMovie : leaderVector)
	{
		cout << "Player: " << thisMovie.getName() << "| Score: " << thisMovie.getScore() << endl;
	}
	cout << "There are " << leaderVector.size() << " elements in the array" << endl;
}