#pragma once
#include "functions.h"
class LeaderBoard
{
public:
	LeaderBoard();
	
	void setName(string name);
	string getName();

	void setScore(int score);
	int getScore();

private:
	string m_name;
	int m_score;
};